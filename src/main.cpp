/*
 * Keypad with PCF8574 test
 *
 */

#include <Arduino.h>
#include "PCF8574.h"
#include "Keypad.h"

#define I2C_SDA 0
#define I2C_SCL 2
#define PCM8574_ADRESS 0x38

PCF8574 gpio(PCM8574_ADRESS,I2C_SDA,I2C_SCL),*P;

String msg;

const byte ROWS = 4; //four rows
const byte COLS = 4; //four columns
//define the symbols on the buttons of the keypads
char hexaKeys[ROWS][COLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};
byte rowPins[ROWS] = {0,1,2,3}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {4,5,6,7}; //connect to the column pinouts of the keypad

Keypad customKeypad = Keypad( makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS); 

boolean test = 0;

void setup() {

  Serial.begin(115200);
  Serial.println("...starting...");

  delay(100);

  // add the gpio instance to the keypad
  P = &gpio;
  customKeypad.add_gpio(P);

  Serial.println("setup pin schema");
  delay(100);

  gpio.pinMode(P0, INPUT_PULLUP);
  gpio.pinMode(P1, INPUT_PULLUP);
  gpio.pinMode(P2, INPUT_PULLUP);
  gpio.pinMode(P3, INPUT_PULLUP);
  gpio.pinMode(P4, OUTPUT);
  gpio.pinMode(P5, OUTPUT);
  gpio.pinMode(P6, OUTPUT);
  gpio.pinMode(P7, OUTPUT);

  Serial.println("start PCF8574");
  delay(100);
	if (gpio.begin()){
		Serial.println("OK");
	} else {
		Serial.println("KO");
	}
  
  Serial.println("setup done -> go loop");
  delay(100);
}

void loop() {

  // single key functionality
  char customKey = customKeypad.getKey();
  //Serial.println(customKey);
  if (customKey){
    Serial.println(customKey);
  }
/*
  // more advanced keyboard handling with states of key
  if (customKeypad.getKeys())
  {
    for (int i=0; i<LIST_MAX; i++)   // Scan the whole key list.
    {
      if ( customKeypad.key[i].stateChanged )   // Only find keys that have changed state.
      {
        switch (customKeypad.key[i].kstate) {  // Report active key state : IDLE, PRESSED, HOLD, or RELEASED
          case PRESSED:
            msg = " PRESSED.";
            break;
          case HOLD:
            msg = " HOLD.";
            break;
          case RELEASED:
            msg = " RELEASED.";
            break;
          case IDLE:
            msg = " IDLE.";
        }
        Serial.print("Key ");
        Serial.print(customKeypad.key[i].kchar);
        Serial.println(msg);
      }
    }
  }
*/
}