## Keypad example for a Keypad connected to a PCF8574 portexpander

This example uses a modified clone of: https://github.com/Nullkraft/Keypad.
You can find my modified library at https://gitlab.com/lnetwalker/keypad

**Author:** *Hartmut Eilers*

added support for keypads connected to a PCF8574 port expander. Tested with ESP01 controler and PlatformIO environment.

The picture shows an ESP01 with PCF8574 connected via I2C, the keypad and the programming adapter.

![pcf8574 with keypad](./Keypad_pcf8574.jpg)

in your platformio.ini you need settings like:

    [env:esp01]
    platform = espressif8266
    board = esp01
    framework = arduino

    lib_deps = 
        https://github.com/xreef/PCF8574_library.git
        https://gitlab.com/lnetwalker/keypad.git#pcf8574

    lib_ldf_mode = deep

    build_flags = -DPCM8574

    upload_port = COM6
    monitor_port = COM6
    monitor_speed = 115200

important are the **build_flags** and the **branch** behind the path to the repository.

see: https://gitlab.com/lnetwalker/keypad_example for an example.
